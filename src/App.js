import React from 'react';
import InstagramLogin from 'react-instagram-login';
import Amplify, { Auth, API } from 'aws-amplify'; 
import axios from 'axios'
import './App.css';
import config from "./config"


Amplify.configure({
  Auth: {
    mandatorySignIn: true,
    region: config.AWS_REGION,
    //userPoolId: "us-east-1_Nl3mu9W5l",
    identityPoolId: config.AWS_IDENTITY_POOL,
   // userPoolWebClientId: "5uhuv59i7jj3uos394eqpairut"
  },
  API: {
    endpoints: [
      {
        name: "test",
        endpoint: config.API_GATEWAY + "test",
        region: config.AWS_REGION
      },
      {
        name: "userprofile",
        endpoint: config.API_GATEWAY + "userprofile",
        region: config.AWS_REGION
      }
    ]
  }
});


class App extends React.Component { 


   constructor(props) {
     super(props)
     this.state = { 
                   user : "",
                   isAuthenticatedWithAWS : false,
                   errorAuth : "",
                   apiMsg : "",
                   instagram : {}
                  }
   }



   configureAmplify = (token)=> {

        Auth.federatedSignIn("developer", {token: token.Token , identity_id : token.IdentityId})
        .then(cred => {
            // If success, you will get the AWS credentials
            return Auth.currentAuthenticatedUser();
        }).then(user => {
            // If success, the user object you passed in Auth.federatedSignIn
           this.setState({isAuthenticatedWithAWS : true})
        }).catch(e => {
            console.log(e)
            this.setState({errorAuth : e.stack})
        });
  }  


  responseInstagram = (response) => {
    console.log(response);
    
    axios.post(config.INSTAGRAN_VALIDATION_ENDPOINT, {code : response})
    .then((response) =>{
    
        console.log(response.data)
        this.setState({user : response.data.instagram.user.full_name,
                       instagram : response.data.instagram.user})
        this.configureAmplify(response.data.aws)
        return
    })
    .catch((error) => {
        console.log("error", error)
        this.setState({errorAuth : error.stack})
        return
    })
  }

  responseFailure = (response) => {
    console.log(response);
  }

  callTestApi = () => {

    console.log("state", this.state)
    let data = {id : this.state.instagram.id,
      userName : this.state.instagram.full_name,
      instaUsername : this.state.instagram.username,
    sex : "Masculino"}

console.log("data", data)

    API.post("userprofile", "", {body : data })

    .then((response) =>{
        this.setState({apiMsg : response})
    })
    .catch((error) => {
        this.setState({apiMsg : error.stack})
    })
  }

  render(){
    return (
      <div className="App">
  
        <h2>Bem Vindo ao APP</h2>

        <p>Usuario Logado: {this.state.user}</p>
        <p>Autenticado com AWS: {this.state.isAuthenticatedWithAWS ? "Sim" : "Nao"}</p>
        <p>Alerta/Erro: {this.state.errorAuth}</p>


  
        <InstagramLogin
            clientId= {config.INSTAGRAN_CLIENT_ID}
            buttonText="Login com Instagram"
            onSuccess={this.responseInstagram}
            onFailure={this.responseFailure}
        />

        <p>
        <button type="button" onClick={this.callTestApi}>Chama test API</button>
        </p>

        <p>Retorno API: {this.state.apiMsg}</p>

      </div>
    );
  }
  
}

export default App;
